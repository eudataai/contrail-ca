package eu.contrail.security;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import eu.emi.security.authn.x509.impl.CertificateUtils;
import eu.emi.security.authn.x509.impl.CertificateUtils.Encoding;
import eu.emi.security.authn.x509.impl.KeyAndCertCredential;
import eu.unicore.security.canl.AuthnAndTrustProperties;
import eu.unicore.security.canl.CredentialProperties;
import eu.unicore.security.canl.TruststoreProperties;
import eu.unicore.util.Log;
import eu.unicore.util.configuration.ConfigurationException;
import eu.unicore.util.configuration.DocumentationReferenceMeta;
import eu.unicore.util.configuration.DocumentationReferencePrefix;
import eu.unicore.util.configuration.PropertyMD;
import eu.unicore.util.httpclient.DefaultClientConfiguration;

public class CAServerConfiguration extends CAPropertiesHelper {
	private static final Logger log = Logger.getLogger(CAServerConfiguration.class);

	public enum VerificationProtocol {
		mitre, unity, internal
	};

	public static final int DEFAULT_CACHE_TTL = 60;

	@DocumentationReferencePrefix
	public static final String CA_PREFIX = "ca.";

	public static final String ISSUER_EMBED_ASSERTION = "issuer.embedAssertion";
	
	public static final String UNITY_ADDRESS = "unity.address";

	public static final String UNITY_USER = "unity.username";

	public static final String UNITY_PASSWORD = "unity.password";
	
	public static final String UNITY_QUERY_IDENTIFIER = "unity.identifier";

	public static final String ISSUER_KEY = "issuer.privateKey";

	public static final String ISSUER_KEY_PASS = "issuer.privateKeyPassword";

	public static final String ISSUER_CERT = "issuer.publicKey";

	public static final String GEN_CERT_LIFETIME = "issuer.generatedCertificateLifetime";

	public static final String OAUTH_TOKEN_ENDPOINT_ADDRESS = "oauth.verifyTokenEndpointUri";

	public static final String OAUTH_TOKEN_PROTOCOL = "oauth.verifyTokenProtocol";

	@DocumentationReferenceMeta
	public final static Map<String, PropertyMD> meta = new HashMap<String, PropertyMD>();

	static {
		meta.put(
				UNITY_ADDRESS,
				new PropertyMD(
						"https://localhost:2443/soapidp/saml2idp-soap/AssertionQueryService")
						.setDescription("Address to the Unity's Assertion Query Web Service"));

		meta.put(
				UNITY_USER,
				new PropertyMD()
						.setDescription("Username of this CA server in Unity database to request user attributes on her behalf"));

		meta.put(
				UNITY_PASSWORD,
				new PropertyMD()
						.setDescription("Username of this CA server in Unity database to request user attributes on her behalf"));
		
		meta.put(
				UNITY_QUERY_IDENTIFIER,
				new PropertyMD("userName")
						.setDescription("The property represents an identifier to query the Unity service for user's attributes (as a saml assertion)")
						);

		meta.put(
				ISSUER_KEY,
				new PropertyMD().setDescription(
						"Location to CA private key file (in PEM format)")
						.setPath());

		meta.put(
				ISSUER_KEY_PASS,
				new PropertyMD()
						.setDescription("Password of CA private key file")
						.setSecret().setHidden());

		meta.put(
				ISSUER_CERT,
				new PropertyMD()
						.setDescription(
								"Location to CA public key file (in PEM format). The certificate should be placed inside Unity's trust-store")
						.setPath());

		meta.put(
				GEN_CERT_LIFETIME,
				new PropertyMD()
						.setDescription(
								"Certificate lifetime")
						.setLong().setBounds(60, 259200).setDefault("43200"));

		meta.put(
				OAUTH_TOKEN_ENDPOINT_ADDRESS,
				new PropertyMD()
						.setDescription("OAuth-AS token endpoint (http://<hostname>:<port>/oauth-as/r/access_token/check)"));

		meta.put(
				OAUTH_TOKEN_PROTOCOL,
				new PropertyMD()
						.setDescription("OAuth-AS token validation endpoint (http://<hostname>:<port>/oauth-as/r/access_token/check)"));
		
		meta.put(ISSUER_EMBED_ASSERTION, new PropertyMD()
						.setDescription(
								"Flag whether the user assertion from unity has to be embedded")
						.setBoolean().setDefault("false"));
	}

	public CAServerConfiguration(Properties properties)
			throws ConfigurationException {
		super(CA_PREFIX, properties, meta, log);

	}

	public String getUnityAddress() {
		return getValue(UNITY_ADDRESS);
	}

	public String getUnityUsername() {
		return getValue(UNITY_USER);
	}

	public String getUnityPassword() {
		return getValue(UNITY_PASSWORD);
	}

	public KeyAndCertCredential getKeyAndCertCredential() {
		KeyAndCertCredential cred = null;

		InputStream isKey;
		try {
			isKey = new FileInputStream(getFileValue(ISSUER_KEY, false));

			PrivateKey pk = CertificateUtils.loadPrivateKey(isKey,
					Encoding.PEM, getValue(ISSUER_KEY_PASS).toCharArray());

			InputStream isCert = new FileInputStream(getFileValue(ISSUER_CERT,
					false));
			X509Certificate caCert = CertificateUtils.loadCertificate(isCert,
					Encoding.PEM);

			if (isKey != null)
				isKey.close();
			if (isCert != null)
				isCert.close();

			cred = new KeyAndCertCredential(pk,
					new X509Certificate[] { caCert });

		} catch (FileNotFoundException e) {
			log.error("Cannot find CA credentials", e);
		} catch (IOException e) {
			log.error("Error in loading/reading CA credentials", e);
		} catch (KeyStoreException e) {
			log.error("Invalid CA keystore or public key", e);
		}

		return cred;
	}
	
	public Boolean embedAssertion() {
		return getBooleanValue(ISSUER_EMBED_ASSERTION);
	}

	public String getIssuerCertFilePath() {
		return getValue(ISSUER_CERT);
	}

	public String getIssuerKeyFilePath() {
		return getValue(ISSUER_KEY);
	}

	public String getIssuerKeyPasswrd() {
		return getValue(ISSUER_KEY_PASS);
	}
	
	public String getUnityQueryIdentifier() {
		return getValue(UNITY_QUERY_IDENTIFIER);
	}

	public String getCheckTokenEndpointURI() {
		return getValue(OAUTH_TOKEN_ENDPOINT_ADDRESS);
	}
	
	public String getVerifyTokenProtocol() {
		return getValue(OAUTH_TOKEN_PROTOCOL);
	}
	
	public Long getLifetime(){
		return getLongValue(GEN_CERT_LIFETIME);
	}
	
	public AuthnAndTrustProperties getAuthnAndTrustProperties() {
		Properties p = new Properties();
		// Credential Properties
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_FORMAT,
				"PEM");
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_KEY_LOCATION, getValue(ISSUER_KEY));
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_LOCATION, getValue(ISSUER_CERT));
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_PASSWORD, getValue(ISSUER_KEY_PASS));

		// truststore properties
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_TYPE,
				TruststoreProperties.TruststoreType.directory.toString());
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_DIRECTORY_LOCATIONS+"1",
				getValue(ISSUER_CERT));
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_DIRECTORY_ENCODING,
				"PEM");
		
		AuthnAndTrustProperties auth = new AuthnAndTrustProperties(p);
		
		return auth;
	}
	
}
