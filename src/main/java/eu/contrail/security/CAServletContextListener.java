package eu.contrail.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.Security;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import eu.unicore.util.Log;

public class CAServletContextListener implements ServletContextListener{
	protected static Logger log = Log.getLogger("CA",CAServletContextListener.class);
	static{
		Security.addProvider(new BouncyCastleProvider());
	}
	
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		log.info("CA Server version number: "+CAServletContextListener.class.getPackage().getImplementationVersion());
		ServletContext context = servletContextEvent.getServletContext();
		
		String configFilePath = context.getInitParameter("conf-file");
		
			
        if (configFilePath == null) {
            throw new RuntimeException("Missing parameter 'conf-file' in web.xml file.");
        }

        // load configuration file
        File confFile = new File(configFilePath);
        Properties properties = new Properties();
        try {
			properties.load(new FileInputStream(confFile));
		} catch (IOException e) {
			log.error("Error reading 'caserver.properties' file",e);
		}
        CAServerConfiguration config = new CAServerConfiguration(properties);
        context.setAttribute("configuration", config);
	}

	
	public void contextDestroyed(ServletContextEvent sce) {
		
		
	}

}
