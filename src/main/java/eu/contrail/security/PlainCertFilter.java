package eu.contrail.security;

import java.io.IOException;
import java.net.MalformedURLException;
import java.security.Security;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import eu.emi.security.authn.x509.helpers.BinaryCertChainValidator;
import eu.emi.security.authn.x509.impl.KeyAndCertCredential;
import eu.unicore.samly2.assertion.AttributeAssertionParser;
import eu.unicore.samly2.elements.NameID;
import eu.unicore.samly2.exceptions.SAMLValidationException;
import eu.unicore.security.wsutil.samlclient.SAMLAttributeQueryClient;
import eu.unicore.util.Log;
import eu.unicore.util.httpclient.DefaultClientConfiguration;

/***
 * Base filter to set configuration parameters
 * 
 * @author a.memon
 *
 */
public class PlainCertFilter implements Filter {
	private static final Logger log = Log.getLogger("CA", PlainCertFilter.class);

	private ServletContext ctx;

	private DefaultClientConfiguration clientCfg;

	private CAServerConfiguration caConfig;

	private String unityAddress;

	static {
		Security.addProvider(new BouncyCastleProvider());
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		log.info("initialising base filter");
		CAServerConfiguration caConfig = (CAServerConfiguration) filterConfig
				.getServletContext().getAttribute("configuration");
		unityAddress = caConfig.getUnityAddress();
		this.caConfig = caConfig;
		KeyAndCertCredential cred = caConfig.getKeyAndCertCredential();

		// tokenValidator = new
		// TokenValidator(caConfig.getcheckTokenEndpointURI(),
		// cred.getKeyStore(), cred.getKeyPassword(), cred.getKeyManager().get,
		// truststorePass);
		clientCfg = new DefaultClientConfiguration();
		clientCfg.setCredential(cred);

		clientCfg.getHttpClientProperties().setSocketTimeout(3600000);
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		String userName = (String) httpRequest.getParameter(AttributeConstants.USERNAME);
		if (userName != null) {
			httpRequest.setAttribute(AttributeConstants.ASSERTION, getAttributeAssertionParser(userName));	
		}
		chain.doFilter(request, response);
	}

	/***
	 * Calling out to Unity for attributes assertion from Unity
	 * 
	 * @param ownerUuid
	 * @return {@link AttributeAssertionParser}
	 */
	private AttributeAssertionParser getAttributeAssertionParser(
			String userName) {
		AttributeAssertionParser a = null;
		try {
			clientCfg.setHttpUser(caConfig.getUnityUsername());
			clientCfg.setHttpPassword(caConfig.getUnityPassword());
			clientCfg.setSslAuthn(false);
			clientCfg.setHttpAuthn(true);
			clientCfg.setSslEnabled(true);
			clientCfg.setValidator(new BinaryCertChainValidator(true));
			SAMLAttributeQueryClient client = new SAMLAttributeQueryClient(
					unityAddress, clientCfg);

			a = client.getAssertion(new NameID(userName, caConfig.getUnityQueryIdentifier()),
					null);

		} catch (MalformedURLException e) {
			log.warn("Invalid Unity URL", e);
			return null;

		} catch (SAMLValidationException e) {
			log.warn("Error in validating retrieved SAML Assertion from Unity",e);
			return null;
		} 

		return a;
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

}
