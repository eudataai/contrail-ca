/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.contrail.security;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringReader;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.security.auth.x500.X500Principal;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpResponse;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.PKCS10CertificationRequest;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;

import xmlbeans.org.oasis.saml2.assertion.AssertionDocument;
import xmlbeans.org.oasis.saml2.assertion.AssertionType;
import eu.contrail.security.servercommons.CAUtils;
import eu.emi.security.authn.x509.impl.CertificateUtils;
import eu.emi.security.authn.x509.impl.KeyAndCertCredential;
import eu.emi.security.authn.x509.impl.KeystoreCertChainValidator;
import eu.emi.security.authn.x509.impl.KeystoreCredential;
import eu.emi.security.authn.x509.impl.PEMCredential;
import eu.unicore.samly2.assertion.AttributeAssertionParser;
import eu.unicore.util.Log;
import eu.unicore.util.httpclient.DefaultClientConfiguration;

/**
 * @author ijj
 * @author a.memon
 * 
 */
public class DelegatedUserCertServletOAuth extends HttpServlet {

	private static final long serialVersionUID = -1L; // Stop FindBugs
														// complaining
	private static final Logger log = Logger.getLogger(DelegatedUserCertServletOAuth.class);

	/*
	 * Static variables only set in threadsafe 'init' method
	 */
	private static boolean debug = false;
	private static String daysString;
	private static int days;
	private static final int DEFAULT_LIFETIME_HOURS = 12;
	private static String hoursString;
	private static int hours;
	private static int minutes = 0;
	private static ServletContext ctx;
	private static PrivateKey issuerKey;
	private static String issuerKeyPairFilename;
	private static char[] issuerKeyPairPassword = null;
	private static X509Certificate issuerCertificate;
	private static String issuerCertificateFilename;
	private static String allowedCNs;
	private static String issuerName;
	private static PEMCredential pem;
	/*
	 * Variable serialNumber is protected from concurrent update by a synchronized
	 * block
	 */
	private static BigInteger serialNumber;

	// <editor-fold defaultstate="collapsed"
	// desc="HttpServlet methods. Click on the + sign on the left to edit the
	// code.">

	/**
	 * Handles the HTTP <code>GET</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws javax.servlet.ServletException
	 *             if a servlet-specific error occurs
	 * @throws java.io.IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// processRequest(request, response);
		processWithCanl(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws javax.servlet.ServletException
	 *             if a servlet-specific error occurs
	 * @throws java.io.IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// processRequest(request, response);
		processWithCanl(request, response);
	}

	protected void processWithCanl(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		PrintWriter out = response.getWriter();
		String uuid;
		AttributeAssertionParser assertion = null;
		AssertionDocument ad = null;
		String commonName = null;
		String dn = null;
		try {
			Object assertionObj = request.getAttribute(AttributeConstants.ASSERTION);
			
			if (assertionObj != null) {
				assertion = (AttributeAssertionParser) assertionObj;
				if (assertion != null) {
					ad = assertion.getXMLBeanDoc();
					// AssertionType at = ad.getAssertion();
					// at.setAuthnStatementArray(null);
					// at.setConditions(null);
					// at.setAuthzDecisionStatementArray(null);
					// at.setAuthzDecisionStatementArray(null);
					// at.setAdvice(null);
					// at.setSignature(null);
					commonName = assertion.getSubjectName();
					log.trace(assertion.getXMLBeanDoc());
				}

			}

			uuid = (String) request.getAttribute("uuid");
			commonName = (String) request.getAttribute("cn");
			dn = (String) request.getAttribute("dn");
			
			response.setContentType("text/plain");
			String strCsr = request.getParameter(AttributeConstants.CSR_HEADER_NAME);
			KeyAndCertCredential cred = (KeyAndCertCredential) request
					.getAttribute(AttributeConstants.ISSUER_CREDENTIAL);

			Long lifetime = (Long) request.getAttribute("lifetime");

			org.bouncycastle.pkcs.PKCS10CertificationRequest csr = null;
			if (!strCsr.isEmpty()) {
				log.debug("CSR is found in the request");
				log.debug(strCsr);
				csr = new org.bouncycastle.pkcs.PKCS10CertificationRequest(CAUtils.convertRsaPemToDer(strCsr));
			} else {
				log.debug("CSR is null");
				throw new Exception("CSR is null");
			}

			CAUtils utils = new CAUtils(csr, cred);
			String certString = null;
			
			if (assertion != null) {
				certString = utils.generateShortLivedCertificate(ad, uuid, commonName, dn, lifetime);
			} else {
				certString = utils.generateShortLivedCertificate(uuid, commonName, dn, lifetime);
			} 

			out = response.getWriter();
			out.write(certString);

			if (debug) {
				log.debug("DUCS: Wrote cert");
			}
		} catch (Exception e) {
			log.error("Error in signing certificate", e);
		} finally {
			if (out != null) {

				try {
					out.close();
				} catch (Exception ex) {
					;
				}
			}
		}
	}

	/**
	 * Returns a short description of the servlet.
	 * 
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Servlet to generate Certificate on the basis of an OAuth access token";
	}// </editor-fold>

	private static int paramsMissing = 0;

	private static void getInitParams(final ServletConfig config) {

		debug = Boolean.valueOf(config.getInitParameter("debug"));

		allowedCNs = config.getInitParameter("allowedCNs");

		issuerKeyPairFilename = config.getInitParameter("issuerKeyPairFilename");

		if (issuerKeyPairFilename != null) {

			if (debug) {

				ctx.log(String.format("DUCS: issuerKeyPairFilename = %s.", issuerKeyPairFilename));
			}

		} else {

			paramsMissing++;
			ctx.log("DUCS: Cannot read property issuerKeyPairFilename.");

		}

		final String keyPass = config.getInitParameter("issuerKeyPairPassword");

		if (keyPass != null && !keyPass.equals("")) {

			issuerKeyPairPassword = keyPass.toCharArray();

		} else {

			ctx.log("DUCS: Cannot read property issuerKeyPairPassword.");
			/*
			 * Don't incremenet paramsMissing as it is deprecated and need not be set
			 */

		}

		if (debug) {

			ctx.log(String.format("DUCS: issuerKeyPairPassword is %s",
					issuerKeyPairPassword == null ? "not set." : "set, but not logged here."));

		}

		issuerCertificateFilename = config.getInitParameter("issuerCertificateFilename");

		if (issuerCertificateFilename != null) {

			if (debug) {

				ctx.log(String.format("DUCS: issuerCertificateFilename = %s.", issuerCertificateFilename));
			}

		} else {

			paramsMissing++;
			ctx.log("DUCS: Cannot read property issuerCertificateFilename.");

		}

	}

	@Override
	public void init(final ServletConfig config) throws ServletException {
		Security.addProvider(new BouncyCastleProvider());
		ctx = config.getServletContext();
		ctx.log("DUCS: Starting");
		getInitParams(config);
		// DefaultClientConfiguration clientCfg;
		// String keystoreFile = ctx.getInitParameter("issuerKeyPairFilename");
		// String keystorePass = ctx.getInitParameter("issuerKeyPairPassword");
		// String certFile = ctx.getInitParameter("issuerCertificateFilename");
		serialNumber = BigInteger.valueOf(System.currentTimeMillis());
		//
		// try {
		// pem = new PEMCredential(new FileInputStream(new File(
		// issuerKeyPairFilename)), new FileInputStream(new
		// File(issuerCertificateFilename)),
		// issuerKeyPairPassword);
		//
		// ctx.log("the pem credential: "+pem.getCertificate());
		//
		// } catch (KeyStoreException e) {
		// e.printStackTrace();
		// } catch (CertificateException e) {
		// e.printStackTrace();
		// } catch (FileNotFoundException e) {
		// e.printStackTrace();
		// } catch (IOException e) {
		// e.printStackTrace();
		// }

		// clientCfg = new DefaultClientConfiguration();
		// clientCfg.setCredential(new KeystoreCredential(
		// keystoreFile,
		// keystorePass.toCharArray(), keystorePass.toCharArray(), "ca-server",
		// "JKS"));
		// clientCfg.setValidator(new KeystoreCertChainValidator(
		// truststoreFile, truststorePass.toCharArray(), "JKS", -1));
	}

}
