package eu.contrail.security;

import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import eu.unicore.util.configuration.ConfigurationException;
import eu.unicore.util.configuration.PropertiesHelper;
import eu.unicore.util.configuration.PropertyMD;

/**
 * Properties helper to configure CA server from a properties file on Web server
 * start-up
 * */
public class CAPropertiesHelper extends PropertiesHelper {
		
	public CAPropertiesHelper(String prefix, Properties properties,
			Map<String, PropertyMD> propertiesMD, Logger log)
			throws ConfigurationException {
		super(prefix, properties, propertiesMD, log);		
	}

}
