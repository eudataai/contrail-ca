package eu.contrail.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import xmlbeans.org.oasis.saml2.assertion.AssertionDocument;
import eu.contrail.security.servercommons.CAUtils;
import eu.emi.security.authn.x509.impl.KeyAndCertCredential;
import eu.unicore.samly2.assertion.AttributeAssertionParser;
import eu.unicore.util.Log;

public class DelegatedUserCertServlet extends HttpServlet {
	private static final long serialVersionUID = -1L; // Stop FindBugs
	private static final Logger log = Log.getLogger("CA",
			DelegatedUserCertServlet.class);
	public static final String CSR_HEADER_NAME = "certificate_request";
	public static final String ISSUER_CREDENTIAL = "credential";
	private ServletContext ctx;
	CAServerConfiguration caConfig;
	@Override
	public void init(ServletConfig config) throws ServletException {
		ctx = config.getServletContext();
		caConfig = (CAServerConfiguration) ctx.getAttribute("configuration");
	}
	
	/**
	 * Handles the HTTP <code>POST</code> method.
	 * 
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws javax.servlet.ServletException
	 *             if a servlet-specific error occurs
	 * @throws java.io.IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// processRequest(request, response);
		processWithCanl(request, response);
	}

	protected void processWithCanl(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		PrintWriter out = response.getWriter();
		try {
			AttributeAssertionParser assertion = (AttributeAssertionParser) request
					.getAttribute("assertion");
			AssertionDocument ad = null;
			String uuid = null;
			String username = (String) request.getParameter(AttributeConstants.USERNAME);
			if (assertion != null) {
				 ad = assertion.getXMLBeanDoc();
				// AssertionType at = ad.getAssertion();
					// at.setAuthnStatementArray(null);
					// at.setConditions(null);
					// at.setAuthzDecisionStatementArray(null);
					// at.setAuthzDecisionStatementArray(null);
					// at.setAdvice(null);
					// at.setSignature(null);
					
					uuid = (String) request.getAttribute("userName");
					
					//username = assertion.getAttribute("unity:identity:userName").getStringValues().get(0);
			} 
			
			
			
			

			response.setContentType("text/plain");
			String strCsr = request.getParameter(CSR_HEADER_NAME);
			KeyAndCertCredential cred = caConfig.getKeyAndCertCredential();
			
			Long lifetime = (Long) request.getAttribute(CAServerConfiguration.GEN_CERT_LIFETIME);
			
			org.bouncycastle.pkcs.PKCS10CertificationRequest csr = null;
			if (!strCsr.isEmpty()) {
				log.debug("CSR is found in the request");
				log.debug(strCsr);
				csr = new org.bouncycastle.pkcs.PKCS10CertificationRequest(
						CAUtils.convertRsaPemToDer(strCsr));
			} else {
				log.debug("CSR is null");
				throw new Exception("CSR is null");
			}

			CAUtils utils = new CAUtils(csr, cred);
			String certString = null;
			//FIXIT dn value
			if (ad != null) {
				certString = utils.generateShortLivedCertificate(
						ad, null, username, null, lifetime);	
			} else {
				certString = utils.generateShortLivedCertificate(
						uuid, username, null, lifetime);
			}
			
			out = response.getWriter();
			out.write(certString);

			if (log.isDebugEnabled()) {
				log.debug("DUCS: Wrote cert");
			}
		} catch (Exception e) {
			log.error("Error in signing certificate", e);
		} finally {
			if (out != null) {

				try {
					out.close();
				} catch (Exception ex) {
					;
				}
			}
		}
	}
}
