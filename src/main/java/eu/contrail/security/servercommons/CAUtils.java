package eu.contrail.security.servercommons;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.security.auth.x500.X500Principal;

import org.apache.log4j.Logger;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlString;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.util.encoders.Base64;

import xmlbeans.org.oasis.saml2.assertion.AssertionDocument;
import xmlbeans.org.oasis.saml2.assertion.AttributeStatementType;
import xmlbeans.org.oasis.saml2.assertion.AttributeType;
import eu.contrail.security.AttributeConstants;
import eu.emi.security.authn.x509.helpers.CertificateHelpers;
import eu.emi.security.authn.x509.helpers.proxy.ProxySAMLExtension;
import eu.emi.security.authn.x509.helpers.proxy.X509v3CertificateBuilder;
import eu.emi.security.authn.x509.impl.CertificateUtils;
import eu.emi.security.authn.x509.impl.CertificateUtils.Encoding;
import eu.emi.security.authn.x509.impl.KeyAndCertCredential;
import eu.emi.security.authn.x509.impl.OpensslNameUtils;
import eu.emi.security.authn.x509.impl.X500NameUtils;
import eu.emi.security.authn.x509.proxy.CertificateExtension;
import eu.emi.security.authn.x509.proxy.ProxyRequestOptions;
import eu.unicore.util.Log;

public class CAUtils {
	private static final Logger log = Log.getLogger("CA", CAUtils.class);
	private PKCS10CertificationRequest csr;
	private String caCertPath;
	private String caKeyPath;
	private String caKeyPwd;
	private KeyAndCertCredential caCred;

	@Deprecated
	public CAUtils(PKCS10CertificationRequest csr, String caCertPath, String caKeyPath, String caKeyPwd) {
		this.csr = csr;
		this.caCertPath = caCertPath;
		this.caKeyPath = caKeyPath;
		this.caKeyPwd = caKeyPwd;
	}

	public CAUtils(PKCS10CertificationRequest csr, KeyAndCertCredential credential) {
		this.caCred = credential;
		this.csr = csr;
	}

	public String generateShortLivedCertificate(String uuid, String commonName, String dn, Long lifetime) throws Exception {
		return generateShortLivedCertificate(null, uuid, commonName, dn, lifetime);
	}

	public String generateShortLivedCertificate(String username, Long lifetime, String dn) throws Exception {
		return generateShortLivedCertificate(null, null, username, dn, lifetime);
	}

	@Deprecated
	public String _generateShortLivedCertificate(AssertionDocument assertion, String uuid, String username, String dn)
			throws Exception {
		// CSRGenerator csrGen = new CSRGenerator();
		// PKCS10CertificationRequest csr = csrGen.generate(userDN);

		// signing and generating the certificate
		// KeyAndCertCredential caCred = getCACredential(caCertPath, caKeyPath,
		// caKeyPwd);

		KeyAndCertCredential caCred = this.caCred;

		// 15 minutes ago
		final long CredentialGoodFromOffset = 1000L * 60L * 15L;

		final long startTime = System.currentTimeMillis() - CredentialGoodFromOffset;
		// expiration time of 6 hrs
		final long endTime = startTime + 30 * 3600 * 1000;

		X500Name issuerX500Name = CertificateHelpers.toX500Name(caCred.getCertificate().getSubjectX500Principal());
		String issuerName = caCred.getCertificate().getSubjectDN().getName();
		Random rand = new Random();

		// build DN from username

		// X509v3CertificateBuilder certBuilder = new X509v3CertificateBuilder(
		// issuerX500Name, new BigInteger(20, rand), new Date(startTime),
		// new Date(endTime), csr.getSubject(), csr.getSubjectPublicKeyInfo());
		String subjectName = null;
		if (uuid != null) {
			subjectName = String.format("CN=%s, CN=%s", uuid, username);
		} else {
			subjectName = String.format("CN=%s", username);
		}

		X500Principal subjectDN = new X500Principal(subjectName + "," + issuerName);

		X500Name subjectX500Name = CertificateHelpers.toX500Name(subjectDN);

		X509v3CertificateBuilder certBuilder = new X509v3CertificateBuilder(issuerX500Name, new BigInteger(20, rand),
				new Date(startTime), new Date(endTime), subjectX500Name, csr.getSubjectPublicKeyInfo());

		AlgorithmIdentifier sigAlgId = X509v3CertificateBuilder.extractAlgorithmId(caCred.getCertificate());

		String signatureAlgorithm = "SHA1withRSA";

		// add extension
		GeneralNames subjectAltNames = null;
		if (uuid != null) {
			subjectAltNames = new GeneralNames(
					new GeneralName(GeneralName.uniformResourceIdentifier, "urn:uuid:" + uuid));
			certBuilder.addExtension(Extension.subjectAlternativeName, false, subjectAltNames.toASN1Primitive());
		}

		ProxySAMLExtension extValue = null;
		if (assertion != null) {
			extValue = new ProxySAMLExtension(assertion.toString());
			certBuilder.addExtension(new ASN1ObjectIdentifier(ProxySAMLExtension.SAML_OID), false, extValue);
		}

		// X509Certificate certificate = certBuilder.build(caCred.getKey(),
		// sigAlgId, signatureAlgorithm, null, null);

		X509Certificate[] caCertArr = { caCred.getCertificate() };

		ProxyRequestOptions pro = new ProxyRequestOptions(caCertArr, csr);

		ProxyGeneratorHelper helper = new ProxyGeneratorHelper();
		String[] arrDN = null;

		// TODO change static DNs to more dynamic ones
		if ((uuid != null) && (username != null)) {
			arrDN = new String[2];
			arrDN[0] = uuid;
			arrDN[1] = username;
		} else {
			arrDN = new String[1];
			arrDN[0] = username;
		}

		if (uuid != null) {
			pro.addExtension(new CertificateExtension(Extension.subjectAlternativeName.getId(),
					subjectAltNames.toASN1Primitive(), false));
		}

		if (assertion != null) {
			pro.addExtension(new CertificateExtension(ProxySAMLExtension.SAML_OID, extValue, false));
			pro.setSAMLAssertion(assertion.toString());
		}

		X509Certificate[] proxyChain = helper.generate(pro, caCred.getKey(), arrDN);

		X509Certificate certificate = proxyChain[0];

		certificate.checkValidity(new Date());

		certificate.verify(caCred.getCertificate().getPublicKey());

		ByteArrayOutputStream os = new ByteArrayOutputStream();

		CertificateUtils.saveCertificate(os, certificate, CertificateUtils.Encoding.PEM);

		return os.toString("UTF-8");

	}

	public String generateShortLivedCertificate(AssertionDocument assertion, String uuid, String commonName,
			String distinguishedName, Long lifetime) throws Exception {

		KeyAndCertCredential caCred = this.caCred;

		// 15 minutes ago
		final long CredentialGoodFromOffset = 1000L * 60L * 15L;

		final long startTime = System.currentTimeMillis() - CredentialGoodFromOffset;
		// expiration time of 6 hrs
		final long endTime = startTime + 30 * 3600 * 1000;

		X500Name issuerX500Name = CertificateHelpers.toX500Name(caCred.getCertificate().getSubjectX500Principal());
		String issuerName = caCred.getCertificate().getSubjectDN().getName();
		Random rand = new Random();

		String subjectName = null;
		if (commonName != null) {
			subjectName = String.format("CN=%s, CN=%s", uuid, commonName);
		} else {
			subjectName = String.format("CN=%s", uuid);
		}

		X500Principal subjectDN = new X500Principal(subjectName + "," + issuerName);

		X500Name subjectX500Name = CertificateHelpers.toX500Name(subjectDN);

		X509v3CertificateBuilder certBuilder = new X509v3CertificateBuilder(issuerX500Name, new BigInteger(20, rand),
				new Date(startTime), new Date(endTime), subjectX500Name, csr.getSubjectPublicKeyInfo());

		AlgorithmIdentifier sigAlgId = X509v3CertificateBuilder.extractAlgorithmId(caCred.getCertificate());

		String signatureAlgorithm = "SHA1withRSA";

		// add extension
		GeneralNames subjectAltNames = null;
		if (uuid != null) {
			subjectAltNames = new GeneralNames(
					new GeneralName(GeneralName.uniformResourceIdentifier, "urn:uuid:" + uuid));
			certBuilder.addExtension(Extension.subjectAlternativeName, false, subjectAltNames.toASN1Primitive());
		}

		// adding assertion
		ProxySAMLExtension extValue = null;
		if (assertion != null) {
			extValue = new ProxySAMLExtension(assertion.toString());
			certBuilder.addExtension(new ASN1ObjectIdentifier(ProxySAMLExtension.SAML_OID), false, extValue);
		}

		X509Certificate[] caCertArr = { caCred.getCertificate() };

		//building proxy object
		ProxyRequestOptions pro = new ProxyRequestOptions(caCertArr, csr);

		log.debug("Lifetime: " + lifetime);

		pro.setLifetime(lifetime, TimeUnit.SECONDS);

		ProxyGeneratorHelper helper = new ProxyGeneratorHelper();
		
		String[] arrDN = getDN(distinguishedName);
		
		pro.addExtension(new CertificateExtension(Extension.subjectAlternativeName.getId(),
					subjectAltNames.toASN1Primitive(), false));
		

		// block the saml assertion being added in the cert.
		if (assertion != null) {
			pro.addExtension(new CertificateExtension(ProxySAMLExtension.SAML_OID, extValue, false));
			pro.setSAMLAssertion(assertion.toString());
		}

		X509Certificate[] proxyChain = helper.generate(pro, caCred.getKey(), arrDN);

		X509Certificate certificate = proxyChain[0];

		certificate.checkValidity(new Date());

		certificate.verify(caCred.getCertificate().getPublicKey());

		ByteArrayOutputStream os = new ByteArrayOutputStream();

		CertificateUtils.saveCertificate(os, certificate, CertificateUtils.Encoding.PEM);
		String cert = os.toString("UTF-8");
		log.info("cert: " + cert);
		return cert;

	}

	private String[] getDN(AssertionDocument assertion) throws IOException {
		AttributeStatementType[] atArr = assertion.getAssertion().getAttributeStatementArray();
		String[] dnArr = new String[2];
		List<String> lstDn = new ArrayList<String>();
		String dn = null;
		if (atArr.length > 0) {
			AttributeStatementType at = atArr[0];
			AttributeType[] attrTypeArr = at.getAttributeArray();
			for (AttributeType attrType : attrTypeArr) {
				if (attrType.getName().equalsIgnoreCase(AttributeConstants.DN_OID)) {
					XmlString s = (XmlString) attrType.getAttributeValueArray(0);
					dn = OpensslNameUtils.opensslToRfc2253(s.getStringValue());
					lstDn = Arrays.asList(X500NameUtils.getAttributeValues(dn, BCStyle.CN));
				}
			}
			if (dnArr.length > 0) {
				Collections.reverse(lstDn);
				return (String[]) lstDn.toArray();
			}
		}
		return dnArr;
	}

	private String[] getDN(String distinguishedName) throws IOException {
		String[] dnArr = new String[2];
		List<String> lstDn = new ArrayList<String>();
		String dn = null;
		dn = OpensslNameUtils.opensslToRfc2253(distinguishedName);
		lstDn = Arrays.asList(X500NameUtils.getAttributeValues(dn, BCStyle.CN));

		if (dnArr.length > 0) {
			Collections.reverse(lstDn);
			return (String[]) lstDn.toArray();
		}

		return dnArr;

	}

	private KeyAndCertCredential getCACredential(String caCertPath, String caKeyPath, String password)
			throws Exception {
		InputStream isKey = new FileInputStream(caKeyPath);
		PrivateKey pk = CertificateUtils.loadPrivateKey(isKey, Encoding.PEM, password.toCharArray());

		InputStream isCert = new FileInputStream(caCertPath);
		X509Certificate caCert = CertificateUtils.loadCertificate(isCert, Encoding.PEM);

		if (isKey != null)
			isKey.close();
		if (isCert != null)
			isCert.close();

		return new KeyAndCertCredential(pk, new X509Certificate[] { caCert });
	}

	/**
	 * Convert a PEM encoded RSA certificate file into a DER format byte array.
	 *
	 * @param is
	 *            Input stream for PEM encoded RSA certificate data.
	 *
	 * @return The RSA certificate data in DER format.
	 *
	 * @throws IOException
	 */
	public static byte[] convertRsaPemToDer(String pemData) throws IOException {

		// Strip PEM header and footer
		int headerEndOffset = pemData.indexOf('\n');
		int footerStartOffset = pemData.indexOf("-----END");
		String strippedPemData = pemData.substring(headerEndOffset + 1, footerStartOffset - 1);

		// Decode Base64 PEM data to DER bytes
		byte[] derBytes = fromBase64(strippedPemData);
		return derBytes;
	}

	/**
	 * Converts a Base64-encoded string to the original byte data.
	 * 
	 * @param b64Data
	 *            a Base64-encoded string to decode.
	 * 
	 * @return bytes decoded from a Base64 string.
	 */
	public static byte[] fromBase64(String b64Data) {
		byte[] decoded = Base64.decode(b64Data);
		return decoded;
	}
}
