package eu.contrail.security;

public interface AttributeConstants {
	public static final String DN  = "distinguishedName";
	public static final String DN_OID  = "urn:oid:2.5.4.49";
	public static final String ASSERTION = "assertion";
	public static final String CSR_HEADER_NAME = "certificate_request";
	public static final String ISSUER_CREDENTIAL = "credential";
	public static final String USERNAME = "userName";
	public static final String PERSISTENT_ID = "unity:persistent";
	public static final String CN = "cn";
}
