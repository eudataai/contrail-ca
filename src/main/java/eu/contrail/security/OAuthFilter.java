package eu.contrail.security;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.Iterator;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.ws.security.saml.ext.builder.SAML2Constants;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.ow2.contrail.common.oauth.client.ITokenInfo;
import org.ow2.contrail.common.oauth.client.IUserInfo;
import org.ow2.contrail.common.oauth.client.TokenInfo;
import org.ow2.contrail.common.oauth.client.TokenValidationProtocol;
import org.ow2.contrail.common.oauth.rp.ContrailTokenValidator;
import org.ow2.contrail.common.oauth.rp.TokenValidator;
import org.ow2.contrail.common.oauth.rp.TokenValidatorFactory;

import eu.emi.security.authn.x509.helpers.BinaryCertChainValidator;
import eu.emi.security.authn.x509.impl.CertificateUtils;
import eu.emi.security.authn.x509.impl.KeyAndCertCredential;
//import eu.emi.security.authn.x509.impl.CertificateUtils;
import eu.emi.security.authn.x509.impl.KeystoreCredential;
import eu.unicore.samly2.assertion.AttributeAssertionParser;
import eu.unicore.samly2.elements.NameID;
import eu.unicore.samly2.exceptions.SAMLValidationException;
import eu.unicore.security.wsutil.samlclient.SAMLAttributeQueryClient;
import eu.unicore.util.httpclient.DefaultClientConfiguration;
import eu.emi.security.authn.x509.impl.KeystoreCertChainValidator;
import eu.emi.security.authn.x509.impl.CertificateUtils.Encoding;

public class OAuthFilter implements Filter {
	private static final Logger log = Logger.getLogger(OAuthFilter.class);

	private ServletContext ctx;

	private org.ow2.contrail.common.oauth.rp.TokenValidator tokenValidator;

	private DefaultClientConfiguration clientCfg;

	private CAServerConfiguration caConfig;

	private String unityAddress;

	static {
		Security.addProvider(new BouncyCastleProvider());
	}

	public void init(FilterConfig filterConfig) throws ServletException {
		log.info("Initialising OAuth filter");
		CAServerConfiguration caConfig = (CAServerConfiguration) filterConfig.getServletContext()
				.getAttribute("configuration");
		unityAddress = caConfig.getUnityAddress();
		this.caConfig = caConfig;
		KeyAndCertCredential cred = caConfig.getKeyAndCertCredential();

		// tokenValidator = new
		// TokenValidator(caConfig.getcheckTokenEndpointURI(),
		// cred.getKeyStore(), cred.getKeyPassword(), cred.getKeyManager().get,
		// truststorePass);
		clientCfg = new DefaultClientConfiguration();

		clientCfg.setCredential(cred);

		clientCfg.getHttpClientProperties().setSocketTimeout(3600000);

		try {
			tokenValidator = TokenValidatorFactory.newInstance(
					TokenValidationProtocol.fromString(caConfig.getVerifyTokenProtocol()),
					caConfig.getCheckTokenEndpointURI(), caConfig.getAuthnAndTrustProperties());
		} catch (Exception e) {
			log.error("Error in creating token validator", e);
		}

	}

	// remain here for testing purpose
	protected static DefaultClientConfiguration getClientCfg() throws KeyStoreException, IOException {
		DefaultClientConfiguration clientCfg = new DefaultClientConfiguration();
		clientCfg.setCredential(new KeystoreCredential("src/test/resources/tc6.p12", "eudat".toCharArray(),
				"eudat".toCharArray(), null, "PKCS12"));
		clientCfg.setValidator(
				new KeystoreCertChainValidator("src/test/resources/trust.jks", "eudat".toCharArray(), "JKS", -1));
		clientCfg.getHttpClientProperties().setSocketTimeout(3600000);
		return clientCfg;
	}

	// remain here for testing purpose
	protected static DefaultClientConfiguration getTestClientCfg() throws KeyStoreException, IOException {
		DefaultClientConfiguration clientCfg = new DefaultClientConfiguration();

		InputStream isKey = new FileInputStream("src/test/resources/ca.key");
		PrivateKey pk = CertificateUtils.loadPrivateKey(isKey, Encoding.PEM, "eudat".toCharArray());

		InputStream isCert = new FileInputStream("src/test/resources/ca.pem");
		X509Certificate caCert = CertificateUtils.loadCertificate(isCert, Encoding.PEM);

		if (isKey != null)
			isKey.close();
		if (isCert != null)
			isCert.close();

		clientCfg.setCredential(new KeyAndCertCredential(pk, new X509Certificate[] { caCert }));

		clientCfg.setValidator(new BinaryCertChainValidator(true));
		clientCfg.getHttpClientProperties().setSocketTimeout(3600000);

		return clientCfg;
	}

	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
		HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
		try {

			log.debug("HTTP Request:" + httpRequest);
			log.info("Access token received at the CA side: " + httpRequest.getHeader("Authorization"));
			ITokenInfo tokenInfo = tokenValidator.verifyToken(httpRequest);
			log.debug("Token information retreived from UNITY: " + tokenInfo);
			IUserInfo userInfo = null;
			String cn = null;
			if (tokenValidator.getAccessTokenValidationProtocol().equals(TokenValidationProtocol.unity.toString())) {
				userInfo = tokenValidator.getUserInfo(tokenInfo.getAccessToken());
				
				log.debug("User information based on access token retreived from UNITY: " + userInfo);
			} 
			
			if (caConfig.embedAssertion()) {
				httpRequest.setAttribute(AttributeConstants.ASSERTION,
						getAttributeAssertionParser(userInfo.getUserId()));
			}
			
			if (cn != null) {
				httpRequest.setAttribute("cn", userInfo.getName());	
			}
			
			httpRequest.setAttribute("dn", userInfo.getDistinguishedName());
			
			httpRequest.setAttribute("uuid", tokenInfo.getOwnerUuid());

			httpRequest.setAttribute("access_token", tokenInfo.getAccessToken());

			httpRequest.setAttribute("credential", caConfig.getKeyAndCertCredential());

			httpRequest.setAttribute("lifetime", caConfig.getLongValue(CAServerConfiguration.GEN_CERT_LIFETIME));
		} catch (TokenValidator.InvalidOAuthTokenException e) {
			log.error(HttpServletResponse.SC_UNAUTHORIZED, e);
			httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, e.getMessage());
			return;
		} catch (TokenValidator.InvalidCertificateException e) {
			httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, e.getMessage());
			return;
		} catch (Exception e) {
			log.error("Failed to validate OAuth access token: " + e.getMessage(), e);
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			String stacktrace = sw.toString();
			log.error("Stacktrace:" + stacktrace);
			throw new ServletException("Failed to validate OAuth access token.", e);
		}

		filterChain.doFilter(servletRequest, servletResponse);
	}

	/***
	 * Calling out to Unity for attributes assertion from Unity
	 * 
	 * @param ownerUuid
	 * @return {@link AttributeAssertionParser}
	 */
	private AttributeAssertionParser getAttributeAssertionParser(String ownerUuid) {
		AttributeAssertionParser a = null;
		try {
			clientCfg.setHttpUser(caConfig.getUnityUsername());
			clientCfg.setHttpPassword(caConfig.getUnityPassword());
			clientCfg.setSslAuthn(false);
			clientCfg.setHttpAuthn(true);
			clientCfg.setSslEnabled(true);
			clientCfg.setValidator(new BinaryCertChainValidator(true));
			SAMLAttributeQueryClient client = new SAMLAttributeQueryClient(unityAddress, clientCfg);

			a = client.getAssertion(new NameID(ownerUuid, caConfig.getUnityQueryIdentifier()), null);

		} catch (MalformedURLException e) {
			log.warn("Invalid Unity URL", e);
		} catch (SAMLValidationException e) {
			log.warn("Error in validating retrieved SAML Assertion from Unity", e);
		}

		return a;
	}

	public void destroy() {
	}

	// test only
	public static void main(String[] args) {
		Security.addProvider(new BouncyCastleProvider());
		// unity query
		try {
			// String attrWSUrl =
			// "https://localhost:2443/soapidp/saml2idp-soap/AssertionQueryService";
			String attrWSUrl = "https://b2access.eudat.eu:8443/soapidp/saml2idp-soap/AssertionQueryService";
			DefaultClientConfiguration clientCfg = getTestClientCfg();
			clientCfg.setHttpUser("ca");
			clientCfg.setHttpPassword("a");
			clientCfg.setHttpPassword("]bE6mE/");
			clientCfg.setSslAuthn(false);
			clientCfg.setHttpAuthn(true);
			clientCfg.setSslEnabled(true);
			SAMLAttributeQueryClient client = new SAMLAttributeQueryClient(attrWSUrl, clientCfg);
			// AttributeAssertionParser a1 = client.getAssertion(new
			// NameID("7f99a97e-c6be-42ea-9242-ac211e4b19be",
			// "unity:persistent"),
			// null);

			// AttributeAssertionParser a1 = client.getAssertion(new NameID("certclient",
			// "unity:userName"), null);

			AttributeAssertionParser a1 = client
					.getAssertion(new NameID("9ce6eb38-6c28-4280-9869-e961d5d6c8ec", "unity:persistent"), null);

			// AttributeAssertionParser a1 = client.getAssertion(new
			// NameID("eaf9e184-2bf5-49b4-8648-c1425fbcc607",
			// "unity:identifier"),
			// null);

			// AttributeAssertionParser a = client.getAssertion(new
			// NameID("CN=User,O=JSC", SAMLConstants.NFORMAT_DN),
			// null);

			// AttributeAssertionParser a = client.getAssertion(new
			// NameID("0a545e43-9579-4dbf-9421-c7a72acc08bc",
			// SAMLConstants.NFORMAT_PERSISTENT),
			// null);

			// AttributeAssertionParser a = client.getAssertion(new NameID("3",
			// SAMLConstants.NFORMAT_),
			// null);

			System.out.println(a1.getXMLBeanDoc());

			// for (ParsedAttribute t : a.getAttributes()) {
			// System.out.println("Name: "+t.getName()+", Value: "+t.getStringValues());
			// }

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAMLValidationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
