package eu.contrail.security;

import javax.security.auth.x500.X500Principal;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;

import eu.emi.security.authn.x509.helpers.CertificateHelpers;
import eu.emi.security.authn.x509.impl.CertificateUtils;
import eu.emi.security.authn.x509.impl.OpensslNameUtils;
import eu.emi.security.authn.x509.impl.X500NameUtils;

public class TestDN {
	public static void main(String[] args) {
		String rfc = "CN=Joe User,O=State University,O=Globus,C=US";
		String openssl = OpensslNameUtils.convertFromRfc2253(rfc, true);
		System.out.println(openssl);
		
		String opensslLegacy = "/C=EU/O=EUDAT/OU=B2ACCESS/CN=9ce6eb38-6c28-4280-9869-e961d5d6c8ec/CN=Alice";
		String d = OpensslNameUtils.opensslToRfc2253(opensslLegacy);
		
		
		String DN = "CN=Joe User,O=State University,O=Globus,C=US";
		
		System.out.println(X500NameUtils.getAttributeValues(d,BCStyle.CN)[0]);
		System.out.println(X500NameUtils.getAttributeValues(d,BCStyle.CN)[1]);
	}
	
	
}
