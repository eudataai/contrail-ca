package eu.contrail.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Random;
import java.util.UUID;

import javax.security.auth.x500.X500Principal;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.xmlbeans.XmlString;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.util.PrivateKeyFactory;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.bc.BcRSAContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;

import xmlbeans.org.oasis.saml2.assertion.AssertionDocument;
import xmlbeans.org.oasis.saml2.assertion.AssertionType;
import xmlbeans.org.oasis.saml2.assertion.AttributeStatementType;
import xmlbeans.org.oasis.saml2.assertion.AttributeType;
import eu.emi.security.authn.x509.helpers.CertificateHelpers;
import eu.emi.security.authn.x509.impl.CertificateUtils;
import eu.emi.security.authn.x509.impl.KeyAndCertCredential;
import eu.emi.security.authn.x509.impl.CertificateUtils.Encoding;

public class TestUtil {
	public final static String caCertPath = "src/test/resources/ca.pem";
	public final static String caKeyPath = "src/test/resources/ca.key";
	public final static String caPwd = "eudat";

	public static KeyAndCertCredential getCACredential() throws Exception {
		InputStream isKey = new FileInputStream(caKeyPath);
		PrivateKey pk = CertificateUtils.loadPrivateKey(isKey, Encoding.PEM,
				caPwd.toCharArray());

		InputStream isCert = new FileInputStream(caCertPath);
		X509Certificate caCert = CertificateUtils.loadCertificate(isCert,
				Encoding.PEM);

		if (isKey != null)
			isKey.close();
		if (isCert != null)
			isCert.close();

		KeyAndCertCredential cred = new KeyAndCertCredential(pk, new X509Certificate[] { caCert });
		
		
		return cred;
	}
	
	
	public static PKCS10CertificationRequest getCSR() throws Exception{
		// 15 minutes ago
		
		final long CredentialGoodFromOffset = 1000L * 60L * 15L; 

		final long startTime = System.currentTimeMillis()
				- CredentialGoodFromOffset;
		//expiration time of 6 hrs
		final long endTime = startTime + 30 * 3600 * 1000;

		String keyLengthProp = "1024";
		int keyLength = Integer.parseInt(keyLengthProp);
		String signatureAlgorithm = "SHA1withRSA";

		KeyAndCertCredential caCred = getCACredential();
		
		//generate key pair with the same algo. the ca is generated with
		KeyPairGenerator kpg = KeyPairGenerator.getInstance(caCred.getKey().getAlgorithm());
		kpg.initialize(keyLength);
		KeyPair pair = kpg.generateKeyPair();

		X500Principal subjectDN = new X500Principal("CN=some dn");
		Random rand = new Random();

		SubjectPublicKeyInfo publicKeyInfo;
		try {
			publicKeyInfo = SubjectPublicKeyInfo
					.getInstance(new ASN1InputStream(pair.getPublic()
							.getEncoded()).readObject());
		} catch (IOException e) {
			throw new InvalidKeyException("Can not parse the public key"
					+ "being included in the short lived certificate", e);
		}
		
		X500Name issuerX500Name = CertificateHelpers.toX500Name(caCred
				.getCertificate().getSubjectX500Principal());

		X500Name subjectX500Name = CertificateHelpers.toX500Name(subjectDN);
		
		PKCS10CertificationRequestBuilder builder = new PKCS10CertificationRequestBuilder(
				subjectX500Name, publicKeyInfo);

		// generating a csr
		AlgorithmIdentifier signatureAi = new AlgorithmIdentifier(OIWObjectIdentifiers.sha1WithRSA);
		AlgorithmIdentifier hashAi = new AlgorithmIdentifier(OIWObjectIdentifiers.idSHA1);
		BcRSAContentSignerBuilder csBuilder = new BcRSAContentSignerBuilder(signatureAi, hashAi);
		AsymmetricKeyParameter pkParam = PrivateKeyFactory.createKey(pair.getPrivate().getEncoded());
		ContentSigner signer = csBuilder.build(pkParam);
		PKCS10CertificationRequest csr = builder.build(signer);
		return csr;
	}
	
	public static AssertionDocument getAssertion(){
		AssertionDocument ad = AssertionDocument.Factory.newInstance();
		AssertionType at = ad.addNewAssertion();
		AttributeStatementType ast = at.addNewAttributeStatement();

		AttributeType attrType = ast.addNewAttribute();
		attrType.setName("userName");
		XmlString username = XmlString.Factory.newInstance();
		username.setStringValue("testUserName");
		attrType.addNewAttributeValue().set(username);
		
		AttributeType uuidAttrType = ast.addNewAttribute();
		uuidAttrType.setName("unity:persistent");
		XmlString uuid = XmlString.Factory.newInstance();
		uuid.setStringValue(UUID.randomUUID().toString());
		uuidAttrType.addNewAttributeValue().set(uuid);
		System.out.println(ad);
		return ad;
	}
	
	public static KeyStore getPKCS12KeyStore(KeyAndCertCredential cred, String alias, String pass) throws Exception{
		KeyStore store = KeyStore.getInstance("PKCS12");
        store.load(null, null);
        store.setKeyEntry(alias, cred.getKey(), pass.toCharArray(), cred.getCertificateChain());
        return store;        
	}
	
}
