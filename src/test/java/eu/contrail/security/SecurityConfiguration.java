package eu.contrail.security;

import java.util.Properties;

import eu.emi.security.authn.x509.X509CertChainValidator;
import eu.emi.security.authn.x509.X509CertChainValidatorExt;
import eu.unicore.security.canl.AuthnAndTrustProperties;
import eu.unicore.security.canl.CredentialProperties;
import eu.unicore.security.canl.TruststoreProperties;

public class SecurityConfiguration {
	public static AuthnAndTrustProperties getAuthnAndTrustProperties() {
		Properties p = new Properties();
		// Credential Properties
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_FORMAT,
				"PEM");
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_KEY_LOCATION, "src/test/resources/ca.key");
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_LOCATION, "src/test/resources/ca.pem");
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_PASSWORD, "eudat");

		// truststore properties
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_TYPE,
				TruststoreProperties.TruststoreType.directory.toString());
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_DIRECTORY_LOCATIONS+"1",
				"src/test/resources/ca.pem");
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_DIRECTORY_ENCODING,
				"PEM");
		
		AuthnAndTrustProperties auth = new AuthnAndTrustProperties(p);
		
		return auth;
	}
	
	public static X509CertChainValidatorExt getValidator() {
		Properties p = new Properties();
		// Credential Properties
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_FORMAT,
				"PEM");
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_KEY_LOCATION, "src/test/resources/ca.key");
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_LOCATION, "src/test/resources/ca.pem");
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_PASSWORD, "eudat");

		// truststore properties
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_TYPE,
				TruststoreProperties.TruststoreType.keystore.toString());
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_KS_PATH,
				"src/test/resources/ca.jks");
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_KS_TYPE,
				"JKS");
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_KS_PASSWORD,
				"eudat");
		
		AuthnAndTrustProperties auth = new AuthnAndTrustProperties(p);
		
		return auth.getValidator();
	}
}
