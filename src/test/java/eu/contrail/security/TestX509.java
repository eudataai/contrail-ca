package eu.contrail.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Random;

import javax.security.auth.x500.X500Principal;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.util.PrivateKeyFactory;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.bc.BcRSAContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;

import eu.contrail.security.servercommons.CAUtils;
import eu.contrail.security.servercommons.ProxyGeneratorHelper;
import eu.emi.security.authn.x509.helpers.CertificateHelpers;
import eu.emi.security.authn.x509.helpers.proxy.ProxyHelper;
import eu.emi.security.authn.x509.helpers.proxy.X509v3CertificateBuilder;
import eu.emi.security.authn.x509.impl.CertificateUtils;
import eu.emi.security.authn.x509.impl.CertificateUtils.Encoding;
import eu.emi.security.authn.x509.impl.KeyAndCertCredential;
import eu.emi.security.authn.x509.impl.X500NameUtils;
import eu.emi.security.authn.x509.proxy.ProxyCertificate;
import eu.emi.security.authn.x509.proxy.ProxyGenerator;
import eu.emi.security.authn.x509.proxy.ProxyRequestOptions;
import eu.emi.security.authn.x509.proxy.ProxyType;

public class TestX509 {
	public static void main(String[] args) throws Exception {
		TestX509 test = new TestX509();
		KeyAndCertCredential cred = test.generateShortLivedCertificate("CN=Test User","src/test/resources/ca.pem","src/test/resources/ca.key","eudat");
		
//		KeyAndCertCredential proxyCred = test.generateProxyCredential("CN=Test User","src/test/resources/ca.pem","src/test/resources/ca.key","eudat");
		
//		OutputStream os = new FileOutputStream(new File("src/test/resources/tmp_x509.pem"));
//		CertificateUtils.saveCertificate(os, cred.getCertificate(), CertificateUtils.Encoding.PEM);
		
//		new X500Principal("/C=DE/L=Juelich/O=FZJ/OU=JSC/CN=dff640d1-75bb-41e6-a3d1-e287f97aaf69/CN=certclient");
//		String dn = "/C=DE/L=Juelich/O=FZJ/OU=JSC/CN=dff640d1-75bb-41e6-a3d1-e287f97aaf69/CN=certclient";
//		dn = dn.replace("/", ",");
//		dn = dn.substring(1);
//		System.out.println(dn);
//		System.out.println(X500NameUtils.getX500Principal(dn));
		
	}
	
	private KeyAndCertCredential generateShortLivedCertificate(String userDN,
			String caCertPath, String caKeyPath, String caPwd) throws Exception {
		// 15 minutes ago
		final long CredentialGoodFromOffset = 1000L * 60L * 15L; 

		final long startTime = System.currentTimeMillis()
				- CredentialGoodFromOffset;
		//expiration time of 6 hrs
		final long endTime = startTime + 30 * 3600 * 1000;

		String keyLengthProp = "1024";
		int keyLength = Integer.parseInt(keyLengthProp);
		String signatureAlgorithm = "SHA1withRSA";

		KeyAndCertCredential caCred = getCACredential(caCertPath, caKeyPath,
				caPwd);
		
		//generate key pair with the same algo. the ca is generated with
		KeyPairGenerator kpg = KeyPairGenerator.getInstance(caCred.getKey().getAlgorithm());
		kpg.initialize(keyLength);
		KeyPair pair = kpg.generateKeyPair();

		X500Principal subjectDN = new X500Principal(userDN);
		Random rand = new Random();

		SubjectPublicKeyInfo publicKeyInfo;
		try {
			publicKeyInfo = SubjectPublicKeyInfo
					.getInstance(new ASN1InputStream(pair.getPublic()
							.getEncoded()).readObject());
		} catch (IOException e) {
			throw new InvalidKeyException("Can not parse the public key"
					+ "being included in the short lived certificate", e);
		}
		
		X500Name issuerX500Name = CertificateHelpers.toX500Name(caCred
				.getCertificate().getSubjectX500Principal());

		X500Name subjectX500Name = CertificateHelpers.toX500Name(subjectDN);
		
		PKCS10CertificationRequestBuilder builder = new PKCS10CertificationRequestBuilder(
				subjectX500Name, publicKeyInfo);

		// generating a csr
		AlgorithmIdentifier signatureAi = new AlgorithmIdentifier(OIWObjectIdentifiers.sha1WithRSA);
		AlgorithmIdentifier hashAi = new AlgorithmIdentifier(OIWObjectIdentifiers.idSHA1);
		BcRSAContentSignerBuilder csBuilder = new BcRSAContentSignerBuilder(signatureAi, hashAi);
		AsymmetricKeyParameter pkParam = PrivateKeyFactory.createKey(pair.getPrivate().getEncoded());
		ContentSigner signer = csBuilder.build(pkParam);
		PKCS10CertificationRequest csr = builder.build(signer);
		
		
		//signing and generating the certificate
		X509v3CertificateBuilder certBuilder = new X509v3CertificateBuilder(
				issuerX500Name, new BigInteger(20, rand), new Date(startTime),
				new Date(endTime), subjectX500Name, csr.getSubjectPublicKeyInfo());
			
		AlgorithmIdentifier sigAlgId = X509v3CertificateBuilder
				.extractAlgorithmId(caCred.getCertificate());

		X509Certificate certificate = certBuilder.build(caCred.getKey(),
				sigAlgId, signatureAlgorithm, null, null);
		
		
		
		certificate.checkValidity(new Date());
		certificate.verify(caCred.getCertificate().getPublicKey());
		
		OutputStream os = new FileOutputStream(new File("src/test/resources/slcs_x509.pem"));
		CertificateUtils.saveCertificate(os, certificate, CertificateUtils.Encoding.PEM);
		
		KeyAndCertCredential result = new KeyAndCertCredential(
				pair.getPrivate(), new X509Certificate[] { certificate,
						caCred.getCertificate() });

		return result;
	}
	
	
	private KeyAndCertCredential generateProxyCredential(String userDN,
			String caCertPath, String caKeyPath, String caPwd) throws Exception {
		// 15 minutes ago
		final long CredentialGoodFromOffset = 1000L * 60L * 15L; 

		final long startTime = System.currentTimeMillis()
				- CredentialGoodFromOffset;
		//expiration time of 6 hrs
		final long endTime = startTime + 30 * 3600 * 1000;

		String keyLengthProp = "1024";
		int keyLength = Integer.parseInt(keyLengthProp);
		String signatureAlgorithm = "SHA1withRSA";

		KeyAndCertCredential caCred = getCACredential(caCertPath, caKeyPath,
				caPwd);
		
		//generate key pair with the same algo. the ca is generated with
		KeyPairGenerator kpg = KeyPairGenerator.getInstance(caCred.getKey().getAlgorithm());
		kpg.initialize(keyLength);
		KeyPair pair = kpg.generateKeyPair();

		X500Principal subjectDN = new X500Principal(userDN);
		Random rand = new Random();

		SubjectPublicKeyInfo publicKeyInfo;
		try {
			publicKeyInfo = SubjectPublicKeyInfo
					.getInstance(new ASN1InputStream(pair.getPublic()
							.getEncoded()).readObject());
		} catch (IOException e) {
			throw new InvalidKeyException("Can not parse the public key"
					+ "being included in the short lived certificate", e);
		}
		
		X500Name issuerX500Name = CertificateHelpers.toX500Name(caCred
				.getCertificate().getSubjectX500Principal());

		X500Name subjectX500Name = CertificateHelpers.toX500Name(subjectDN);
		
		PKCS10CertificationRequestBuilder builder = new PKCS10CertificationRequestBuilder(
				subjectX500Name, publicKeyInfo);

		// generating a csr
		AlgorithmIdentifier signatureAi = new AlgorithmIdentifier(OIWObjectIdentifiers.sha1WithRSA);
		AlgorithmIdentifier hashAi = new AlgorithmIdentifier(OIWObjectIdentifiers.idSHA1);
		BcRSAContentSignerBuilder csBuilder = new BcRSAContentSignerBuilder(signatureAi, hashAi);
		AsymmetricKeyParameter pkParam = PrivateKeyFactory.createKey(pair.getPrivate().getEncoded());
		ContentSigner signer = csBuilder.build(pkParam);
		PKCS10CertificationRequest csr = builder.build(signer);
		
		
		//signing and generating the certificate
		X509v3CertificateBuilder certBuilder = new X509v3CertificateBuilder(
				issuerX500Name, new BigInteger(20, rand), new Date(startTime),
				new Date(endTime), subjectX500Name, csr.getSubjectPublicKeyInfo());
			
		AlgorithmIdentifier sigAlgId = X509v3CertificateBuilder
				.extractAlgorithmId(caCred.getCertificate());

		//proxy code
		X509Certificate[] caCertArr = {caCred.getCertificate()};
		ProxyRequestOptions pro = new ProxyRequestOptions(caCertArr, csr);
		
		ProxyGeneratorHelper helper = new ProxyGeneratorHelper();
		
		String dn = "uuid, username";
		
		String[] arrDn = dn.split(",");
				
		
		X509Certificate[] proxyChain = helper.generate(pro, caCred.getKey(), arrDn);
		
		
		
//				
//		X509Certificate certificate = certBuilder.build(caCred.getKey(),
//				sigAlgId, signatureAlgorithm, null, null);
		
		
		X509Certificate certificate = proxyChain[0];
		
		certificate.checkValidity(new Date());
		certificate.verify(caCred.getCertificate().getPublicKey());
		
		OutputStream os = new FileOutputStream(new File("src/test/resources/tmp_x509.pem"));
		CertificateUtils.saveCertificate(os, certificate, CertificateUtils.Encoding.PEM);
		
		
		
		KeyAndCertCredential result = new KeyAndCertCredential(
				pair.getPrivate(), new X509Certificate[] { certificate,
						caCred.getCertificate() });
		
		System.out.println(result.getCertificate().getIssuerX500Principal().getName("RFC2253"));
		System.out.println(result.getCertificate().getIssuerX500Principal().getName("RFC1779"));
		System.out.println(result.getCertificate().getIssuerX500Principal().getName("CANONICAL"));
		System.out.println(result.getCertificate().getIssuerX500Principal().getName());
		return result;
	}
	
	
	

	private KeyAndCertCredential getCACredential(String caCertPath,
			String caKeyPath, String password) throws Exception {
		InputStream isKey = new FileInputStream(caKeyPath);
		PrivateKey pk = CertificateUtils.loadPrivateKey(isKey, Encoding.PEM,
				password.toCharArray());

		InputStream isCert = new FileInputStream(caCertPath);
		X509Certificate caCert = CertificateUtils.loadCertificate(isCert,
				Encoding.PEM);

		if (isKey != null)
			isKey.close();
		if (isCert != null)
			isCert.close();

		return new KeyAndCertCredential(pk, new X509Certificate[] { caCert });
	}
	
	

}
