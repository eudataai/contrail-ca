package eu.contrail.security;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.message.BasicHeader;

import eu.unicore.util.httpclient.DefaultClientConfiguration;
import eu.unicore.util.httpclient.HttpUtils;
import eu.unicore.util.httpclient.IClientConfiguration;

public class TestFetchSAMLWithToken {
	static String URI = "https://localhost:2443/soapidp/saml2idp-soap/AssertionQueryService";
	static String ACCESS = "JrIwajQi1QfCt0yNjy-RhAvY6wAu3wgm1ibyb1Sk8kU";
	public static void main(String[] args) throws Exception {
		HttpGet httpGet = new HttpGet(URI);
		BasicHeader bh = new BasicHeader("Authorization", "Bearer "
				+ ACCESS);

		httpGet.setHeader(bh);
		IClientConfiguration cfg = new DefaultClientConfiguration(SecurityConfiguration.getValidator(),SecurityConfiguration.getAuthnAndTrustProperties().getCredential());
		
		HttpClient httpClient = HttpUtils.createClient(URI, cfg);
		HttpResponse response = httpClient.execute(httpGet);
		System.out.println(response.getStatusLine().getStatusCode());
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		response.getEntity().writeTo(baos);
		String json = baos.toString();
		System.out.println("Response from Unity: "+json);
	}
}
