package eu.contrail.security;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.bouncycastle.openssl.PEMWriter;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.ow2.contrail.common.oauth.client.utils.CSRGenerator;
import org.ow2.contrail.common.oauth.client.utils.CertUtils;

import eu.unicore.util.httpclient.DefaultClientConfiguration;
import eu.unicore.util.httpclient.HttpUtils;

public class TestUserCertOAuth {
	EmbeddedJetty j = new EmbeddedJetty();
	private static String accessToken = "JM7Gnp8J0qdl3oJOlg6S0d9xgaKTDpb13_cuMn-XV-s";
	@Before
	public void setup() {
		j.start("ca");
	}

	@Test
	public void testGenerateCert() throws Exception {
		// TODO: subject?
		String subject = String.format("CN=%s", "TestUser");

		CSRGenerator csrGen = new CSRGenerator();
		PKCS10CertificationRequest csr = csrGen.generate(subject);
		StringWriter writer = new StringWriter();
		PEMWriter pemWriter = new PEMWriter(writer);
		pemWriter.writeObject(csr);
		pemWriter.flush();
		pemWriter.close();

		HttpPost request = new HttpPost(EmbeddedJetty.baseUrl + "/ca/o/delegateduser");
		request.addHeader("Authorization", String.format("Bearer %s", accessToken));
        request.addHeader("Accept-Encoding", "identity");
		String pemEncoded = writer.toString();
		List<NameValuePair> formParams = new ArrayList<NameValuePair>();
		formParams
				.add(new BasicNameValuePair("certificate_request", pemEncoded));
		
		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formParams,
				"UTF-8");
		request.setEntity(entity);
		DefaultClientConfiguration c = new DefaultClientConfiguration(
				SecurityConfiguration.getAuthnAndTrustProperties()
						.getValidator(), SecurityConfiguration
						.getAuthnAndTrustProperties().getCredential());
		HttpClient client = HttpUtils.createClient(EmbeddedJetty.baseUrl + "/ca/o/delegateduser",
				c);
		HttpResponse response = client.execute(request);
		
		InputStreamReader isr = null;
		isr = new InputStreamReader(response.getEntity().getContent());
		X509Certificate cert = CertUtils.readCertificate(isr);
		System.out.println(cert);
	}

	@After
	public void tearDown() {
		j.stop();
	}

}
