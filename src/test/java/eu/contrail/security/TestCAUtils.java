package eu.contrail.security;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.security.KeyStore;
import java.security.cert.X509Certificate;

import org.apache.commons.io.FileUtils;
import org.bouncycastle.jce.PKCS12Util;
import org.bouncycastle.jce.interfaces.PKCS12BagAttributeCarrier;
import org.bouncycastle.jce.provider.X509CertificateObject;
import org.junit.Test;

import static org.junit.Assert.*;
import eu.contrail.security.servercommons.CAUtils;
import eu.emi.security.authn.x509.impl.CertificateUtils;
import eu.emi.security.authn.x509.impl.FormatMode;
import eu.emi.security.authn.x509.impl.KeyAndCertCredential;

public class TestCAUtils {
	@Test
	public void testGenerateProxyCredential() throws Exception {
		CAUtils ca = new CAUtils(TestUtil.getCSR(), TestUtil.getCACredential());
		String cert = ca.generateShortLivedCertificate(TestUtil.getAssertion(),
				"testUserName", "testUUID", "", 3600L);
		System.out.println(cert);
		assertTrue((cert.startsWith("-----BEGIN CERTIFICATE-----")));
		FileUtils.writeStringToFile(new File("src/test/resources/tmp_x509.pem"), cert);
		X509Certificate x509cert = CertificateUtils.loadCertificate(new ByteArrayInputStream(cert.getBytes()), CertificateUtils.Encoding.PEM);
		System.out.println(x509cert.toString());
	}

}
