# Contrail Federation Certificate Authority Server #

Author: Ian Johnson

Scientific Computing Department,
STFC Rutherford Appleton Laboratory,
Harwell Oxford,
OX11 0QX,
United Kingdom


Author: Ahmed Shiraz Memon

Forschungszentrum Jülich GmbH,
52425, Jülich,
Germany

## DESCRIPTION ##

This component provides the the means of creating the configuration for a Contrail Federation 
Root CA, and a WAR file containing servlets for Tomcat to act as the CA server.

## Build from source ##

The ca server depends on the oauth client library, which can be found inside the contrail-oauth2 repository. The build steps follows:

1. Install Maven 3 and JDK 1.8 or above
2. git clone <REPO_URL>/eudataai/contrail-oauth2.git contrail-oauth2
3. cd contrain-oauth2
4. mvn clean install -DskipTests
5. git clone <REPO_URL>/eudataai/contrail-ca.git contrail-ca
6. cd contrail-ca
7. mvn clean install -DskipTests 

## PROVIDES ##

* <TOMCAT DIR>/webapps/ca.war

* /etc/contrail/ca-server/create-rootca-files.conf
* /etc/contrail/ca-server/tomcat-connector-fragment.xml

* /usr/share/contrail/ca-server/LICENSE.txt
* /usr/share/contrail/ca-server/README.md